//
//  PixelColor.h
//  ImageProcessing
//
//  Created by Nikola Misic on 2/10/14.
//  Copyright (c) 2014 Nikola Misic. All rights reserved.
//

#import <Foundation/Foundation.h>

struct RGBColor {
	NSInteger red;
	NSInteger blue;
	NSInteger green;
	NSInteger alpha;
};

@interface PixelColor : NSObject

-(struct RGBColor)colorAtPoint:(CGPoint)point forImage:(UIImage*)image;
-(struct RGBColor)colorAtPoint:(CGPoint)point forImageNamed:(NSString*)name;

@end
