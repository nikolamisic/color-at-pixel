//
//  PixelColor.m
//  ImageProcessing
//
//  Created by Nikola Misic on 2/10/14.
//  Copyright (c) 2014 Nikola Misic. All rights reserved.
//

#import "PixelColor.h"

@interface PixelColor()

-(NSInteger)integerValueForHexString:(NSString *)string;
-(NSInteger)valueForHex:(NSString *)hex;

@end

@implementation PixelColor

- (NSInteger)integerValueForHexString:(NSString *)string
{
    NSInteger result=0;
    
    NSString *testString = [string uppercaseString];
    
    for (NSInteger i=0; i<string.length; i++) {
        NSInteger bitValue = [self valueForHex:[testString substringWithRange:NSMakeRange(i, 1)]];
        result += bitValue * pow(16, string.length-i-1);
    }
    
    return result;
}

-(NSInteger)valueForHex:(NSString *)hex
{
    if ([hex isEqualToString:@"A"]) {
        return 10;
    }
    else if ([hex isEqualToString:@"B"]) {
        return 11;
    }
    else if ([hex isEqualToString:@"C"]) {
        return 12;
    }
    else if ([hex isEqualToString:@"D"]) {
        return 13;
    }
    else if ([hex isEqualToString:@"E"]) {
        return 14;
    }
    else if ([hex isEqualToString:@"F"]) {
        return 15;
    }
    else
    {
        return hex.integerValue;
    }
}

-(struct RGBColor)colorAtPoint:(CGPoint)point forImageNamed:(NSString *)name
{
	UIImage *image = [UIImage imageNamed:name];
	return [self colorAtPoint:point forImage:image];
}

-(struct RGBColor)colorAtPoint:(CGPoint)point forImage:(UIImage *)image
{
    CGImageRef cgimage = image.CGImage;
    
    size_t width  = CGImageGetWidth(cgimage);
    size_t height = CGImageGetHeight(cgimage);
	
	if (point.x > width || point.y > height) {
		struct RGBColor color = {0,0,0,0};
		return color;
	}
    
    size_t bpr = CGImageGetBytesPerRow(cgimage);
    size_t bpp = CGImageGetBitsPerPixel(cgimage);
    size_t bpc = CGImageGetBitsPerComponent(cgimage);
    size_t bytes_per_pixel = bpp / bpc;
	
	CGDataProviderRef provider = CGImageGetDataProvider(cgimage);
    NSData* data = (id)CFBridgingRelease(CGDataProviderCopyData(provider));
    const uint8_t* bytes = [data bytes];
	
	NSInteger x = point.x;
	NSInteger y = point.y;
	
	const uint8_t* pixel = &bytes[y * bpr + x * bytes_per_pixel];
	
	for(size_t x = 0; x < bytes_per_pixel; x++)
	{
		printf("%d", [self integerValueForHexString:[NSString stringWithFormat:@"%.2X", pixel[x]]]);
		//                printf("%.2X", pixel[x]);
		if( x < bytes_per_pixel - 1 )
			printf(",");
	}
	
	struct RGBColor color;
	color.red = [self integerValueForHexString:[NSString stringWithFormat:@"%.2X", pixel[0]]];
	color.green = [self integerValueForHexString:[NSString stringWithFormat:@"%.2X", pixel[1]]];
	color.blue = [self integerValueForHexString:[NSString stringWithFormat:@"%.2X", pixel[2]]];
	if (bytes_per_pixel > 3) {
		color.alpha = [self integerValueForHexString:[NSString stringWithFormat:@"%.2X", pixel[3]]];
	}
	else
	{
		color.alpha = 1;
	}
	
	return color;
}

@end
